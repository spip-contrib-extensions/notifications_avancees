<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) return;

include_spip('inc/meta');

// Installation et mise à jour
function notifavancees_upgrade($nom_meta_base_version, $version_cible){
	$maj = array();
	
	$maj['create'] = array(
		array('maj_tables', array('spip_notifications_abonnements')),
	);
	
	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

// Désinstallation
function notifavancees_vider_tables($nom_meta_base_version){
	include_spip('base/abstract_sql');
	
	// On efface les tables du plugin
	sql_drop_table('spip_notifications_abonnements');

	// On efface la version entregistrée
	effacer_meta($nom_meta_base_version);
}
