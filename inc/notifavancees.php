<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) return;

function notifavancees_purge_jetons_doublons($racine_fichiers, $time_limit, $nb_max_files = 100) {

	if ($fichiers_jeton = glob($racine_fichiers . "*.txt")) {
		foreach ($fichiers_jeton as $fichier_jeton) {
			if (filemtime($fichier_jeton) < $time_limit) {
				@unlink($fichier_jeton);
				if ($nb_max_files--<=0) {
					return;
				}
			}
		}
	}

}
