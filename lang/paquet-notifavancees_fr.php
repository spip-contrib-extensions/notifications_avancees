<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// N
	'notifavancees_description' => 'Permet une gestion fine des notifications à envoyer avec possibilité de s\'inscrire/se désinscrire et de choisir les modes d\'envois.',
	'notifavancees_slogan' => 'Permet une gestion fine des notifications à envoyer avec possibilité de s\'inscrire/se désinscrire et de choisir les modes d\'envois',
);