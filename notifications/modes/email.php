<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// Envoi le contenu par email
function notifications_modes_email_envoyer_dist($contact, $contenu, $options) {

	$hash_envoi = implode(":", [$contact, $contenu['texte'] ?? '', $contenu['court']]);
	$hash_envoi = md5($hash_envoi);

	$dir_tmp = sous_repertoire(_DIR_TMP, 'notifavancees');
	$file_hash_envoi = $dir_tmp . "email-" . $hash_envoi . ".txt";
	$time_jeton_obsolete = $_SERVER['REQUEST_TIME'] - 24 * 3600;
	// si on a envoyé la même notification il y a moins de 24h, on s'abstient
	if (file_exists($file_hash_envoi) and filemtime($file_hash_envoi) > $time_jeton_obsolete) {
		spip_log("notifications_modes_email_envoyer_dist: BLOCK notif $hash_envoi @ $contact, déjà envoyée", 'notifavancees'. _LOG_DEBUG);
		return false;
	}

	spip_log("notifications_modes_email_envoyer_dist: envoi notif $hash_envoi @ $contact", 'notifavancees'. _LOG_DEBUG);
	@touch($file_hash_envoi);
	include_spip('inc/notifavancees');
	notifavancees_purge_jetons_doublons($dir_tmp . "email-", $time_jeton_obsolete);

	// S'il y a le plugin Facteur, on peut faire un truc plus propre
	if (defined('_DIR_PLUGIN_FACTEUR')) {
		$corps = $contenu;
	} // Sinon c'est juste le texte
	else {
		$corps = $contenu['texte'];
	}

	$envoyer_mail = charger_fonction('envoyer_mail', 'inc/');
	return $envoyer_mail($contact, $contenu['court'], $corps);
}

// Renvoie une adresse e-mail ou rien
function notifications_modes_email_contact_dist($destinataire) {
	include_spip('inc/filtres');

	// Si c'est déjà un mail
	if (email_valide($destinataire)) {
		return $destinataire;
	} // Si c'est un id_auteur
	elseif (intval($destinataire) == $destinataire
		and $destinataire > 0
		and $email = sql_getfetsel('email', 'spip_auteurs', 'id_auteur = '.$destinataire)
	) {
		return $email;
	} // Sinon rien
	else {
		return null;
	}
}
